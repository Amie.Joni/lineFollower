// -----------------------------------------------------------------------------
// In this project i made a line follower 
// Using : arduino, L298N board,Infrared sensor
// -----------------------------------------------------------------------------
#include <L298N.h>
//Pin
//Motor left
const byte ENL  = 9;
const byte INL1 = 10;
const byte INL2 = 11;

//Motor right
const byte ENR  = 5;
const byte INR1 = 6;
const byte INR2 = 7;

//Infrared sensor left and right
const byte IR_R = 2;
const byte IR_L = 3;

//speed
const int MLmin = 70;
const int MRmin = 80;
const int MLmax = 255;
const int MRmax = 255;

//Motors object
L298N motorR(ENR, INR1, INR2);
L298N motorL(ENL, INL1, INL2);

void setup() {
  //IN/OUT

  Serial.begin(9600);

  pinMode(IR_L,INPUT);
  pinMode(IR_R,INPUT);

  motorL.setSpeed(MLmin);
  motorR.setSpeed(MRmin);

  Serial.println("Start");
}

void loop() {
  //motorL.forward();
  //motorR.forward();
  //Read values
  byte L_V = digitalRead(IR_L);
  byte R_V = digitalRead(IR_R);

  //Traitement
  if(L_V == 0 && R_V == 0){
      Serial.println("Start");
      motorL.forward();
      motorR.forward();
    }else if(L_V == 1 && R_V == 0){
      Serial.println("Left side err");
      motorL.stop();
      motorR.stop();
      motorL.backward();
      motorR.backward();
      delay(100);

      while(L_V && !R_V){
        motorR.setSpeed(MRmax);
        motorL.setSpeed(MLmax);//
        motorR.forward();
        motorL.backward();//
        L_V = digitalRead(IR_L);
        R_V = digitalRead(IR_R);
      }

      motorR.setSpeed(MRmin);
      motorL.setSpeed(MLmin);//
    }else if(L_V == 0 && R_V == 1){
      Serial.println("Right side err");
      motorL.stop();
      motorR.stop();
      motorL.backward();
      motorR.backward();
      delay(100);

      while(R_V && !L_V){
        motorL.setSpeed(MLmax);
        motorR.setSpeed(MRmax);//
        motorL.forward();
        motorR.backward();//
        L_V = digitalRead(IR_L);
        R_V = digitalRead(IR_R);
      }

      motorL.setSpeed(MLmin);
      motorR.setSpeed(MRmin);//

    }else{
        Serial.println("End line");
        motorL.stop();
        motorR.stop();
    }
}
